package com.itau.helloworld_spring.repository;
import org.springframework.data.repository.CrudRepository;
import com.itau.helloworld_spring.model.Carro;

public interface CarroRepository extends CrudRepository<Carro, Integer>{

}

package com.itau.helloworld_spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.helloworld_spring.model.Carro;
import com.itau.helloworld_spring.repository.CarroRepository;

@Controller
public class CarroController {
	
	@Autowired
	CarroRepository CarroRepository;
	
	@RequestMapping(path="/carro", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Carro> getCarro() {
		return CarroRepository.findAll();
	}
	
	@RequestMapping(path="/carro/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Carro getCarroById(@PathVariable(value="id") int id) {
		
		Carro carro = new Carro();
		carro.setId(id);
		carro.setAno(2018);
		carro.setCor("Verde");
		carro.setMarca("Honda");
		carro.setModelo("Fit");
		carro.setPlaca("BEZ-1410");
		
		return carro;
	}
	
	@RequestMapping(path="/carro", method=RequestMethod.POST)
	@ResponseBody
	public Carro setCarro(@RequestBody Carro carro) {
		return CarroRepository.save(carro);
	}
	
	@RequestMapping(path="/carro/bom")
	public ResponseEntity<?> eBom(@RequestParam String modelo) {
		if(modelo.equals("Civic")) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.badRequest().build();
	}
	
}
